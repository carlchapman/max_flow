package hw6;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import edu.iastate.cs311.f13.hw6.IGraph;

/**
 * The Class Graph is a concrete implementation of IGraph.
 * 
 *  @author carlchapman
 */
public class Graph implements IGraph {

	/**
	 * All outgoing edges of a vertex are kept in as a set within a map, with
	 * the vertex being the key to get the set. A LinkedHashSet is used because
	 * these will always be iterated over when gotten from the map.
	 */
	private HashMap<String, LinkedHashSet<Pair<String, String>>> edges;

	/**
	 * The set of vertices is kept as a LinkedHashSet, as these will always be
	 * iterated over when used.
	 */
	private LinkedHashSet<String> vertices;

	/**
	 * Instantiates a new graph.
	 */
	public Graph() {
		edges = new HashMap<String, LinkedHashSet<Pair<String, String>>>();
		vertices = new LinkedHashSet<String>();
	}

	@Override
	public Collection<String> getVertices() {
		return vertices;
	}

	@Override
	public Collection<Pair<String, String>> getOutgoingEdges(String v) {
		return edges.get(v) == null ? new LinkedList<Pair<String, String>>() : edges.get(v);
	}

	@Override
	public void addVertex(String v) {
		vertices.add(v);
	}

	@Override
	public void addEdge(Pair<String, String> e) {
		LinkedHashSet<Pair<String, String>> outgoingList = edges.get(e.first);

		// note that a new set for a given vertex (e.first) must be created if
		// not in the map yet.
		if (outgoingList == null) {
			outgoingList = new LinkedHashSet<Pair<String, String>>();
		}
		outgoingList.add(e);
		edges.put(e.first, outgoingList);
	}

	@Override
	public void deleteVertex(String v) {
		vertices.remove(v);
	}

	@Override
	public void deleteEdge(Pair<String, String> e) {
		LinkedHashSet<Pair<String, String>> outgoingList = edges.get(e.first);
		if (outgoingList != null) {
			outgoingList.remove(e);
		}
	}
}
