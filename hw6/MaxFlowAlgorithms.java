package hw6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.UUID;

import edu.iastate.cs311.f13.hw6.IGraph;
import edu.iastate.cs311.f13.hw6.IGraph.Pair;
import edu.iastate.cs311.f13.hw6.IMaxFlowAlgorithms;
import edu.iastate.cs311.f13.hw6.ITopologicalSortAlgorithms.DFSCallback;

public class MaxFlowAlgorithms implements IMaxFlowAlgorithms {

	/**
	 * A switch to turn debug statements on or off. When off, this should not
	 * print anything.
	 */
	private static boolean debug = false;

	/**
	 * Computes the maximal flow that can travel on g from s to t while
	 * constrained by edgeCapacities.
	 * 
	 * <p>
	 * Function: After checking for the trivial case where s==t, the map
	 * containing edgeCapacities is used to construct a residual flow graph that
	 * will be modified to find the max flow. Similarly, a new map of residual
	 * edge flows is constructed.<br>
	 * </br>The residual graph is augmented up to f times using the recursiveDFS
	 * function from the TopologicalSort class and a special processor that is
	 * described in more detail below. Briefly, it builds a list of vertices in
	 * a path when DFS is backtracking from T, and uses that to augment the
	 * graph.<br>
	 * </br>When no more augmentations can be made, a map of the edge weights is
	 * constructed from the residual edge flows and returned.
	 * </p>
	 * <p>
	 * Cost: Creating the residual graph from a list of edges takes O(|E|),
	 * because constant work is done for each edge. Augmenting the residual
	 * graph takes O(|E|*f) in the worst case because at each step, the flow
	 * increases by at least 1, and takes O(|E|) time to do so. Creating a map
	 * of edge weights takes O(|E|) because constant work is done for each edge.
	 * So the overall worst-case running time is O(|E|*f).
	 * </p>
	 * 
	 * @param g
	 *            The graph through which flow will travel.
	 * @param s
	 *            The source vertex.
	 * @param t
	 *            The destination vertex.
	 * @param edgeCapacities
	 *            The capacities of the edges in g. You may assume that: for all
	 *            e, e is a key in edgeCapacities if and only if e is an edge in
	 *            g.
	 * @return A maximal flow.
	 * 
	 * @author carlchapman
	 */
	@Override
	public Map<Pair<String, String>, Integer> maxFlow(IGraph g, String s, String t, Map<Pair<String, String>, Integer> edgeCapacities) {
		// note that the variable g is unnecessary
		// edgeCapacities are sufficient

		// check for trivial graph where s=t;
		final Map<Pair<String, String>, Integer> residualEdgeFlows = new HashMap<Pair<String, String>, Integer>();
		if (s.equals(t)) {
			System.out.println("s equals t.  returning empty result from maxFlow()");
			return residualEdgeFlows;
		}

		// initialize the residualGraph & residualEdgeFlows - O(E) time.
		final IGraph residualGraph = new Graph();
		for (Pair<String, String> edge : edgeCapacities.keySet()) {
			if (debug)
				System.out.println("adding vertices and edge: (" + edge.first + "," + edge.second + ")");

			residualGraph.addVertex(edge.first);
			residualGraph.addVertex(edge.second);
			residualGraph.addEdge(new Pair<String, String>(edge.first, edge.second));
			residualEdgeFlows.put(edge, edgeCapacities.get(edge));
		}

		// augment the residual flow graph O(e) up to f times
		FlowDFSCallback processor = new FlowDFSCallback(residualEdgeFlows, residualGraph, s, t);
		augmentGraph(residualGraph, residualEdgeFlows, s, t, processor);

		// once no augmentations can be made, return max flow edge weights
		Map<Pair<String, String>, Integer> maxFlowEdgeWeights = new HashMap<Pair<String, String>, Integer>(edgeCapacities.size());
		for (Pair<String, String> edge : edgeCapacities.keySet()) {
			Integer maxFlow = residualEdgeFlows.get(new Pair<String, String>(edge.second, edge.first));
			if (maxFlow == null) {
				maxFlowEdgeWeights.put(edge, 0);
			} else {
				maxFlowEdgeWeights.put(edge, maxFlow);
			}
		}

		return maxFlowEdgeWeights;
	}

	/**
	 * Computes the maximal flow that can travel on g from s to t while
	 * constrained by vertexCapacities.
	 * 
	 * <p>
	 * Function: The given graph is transformed into an edge-weighted graph by
	 * setting each edge to the minimum of its endpoint vertex weights. Then the
	 * maxFlow algorithm is run on the new graph and the results are returned.
	 * </p>
	 * <p>
	 * Cost: It takes O(|V|+|E|) to construct the edge-weighted graph, and
	 * O(|E|*f) to evaluate it. So you could say that the running time is
	 * O(Max((|V|+|E|), (|E|*f))), but since that is not the right notation for
	 * big-oh, and we can be sure that both<br>
	 * </br> (|E|*f) < ((|V|+|E|)*f) <br>
	 * </br>AND<br>
	 * </br>(|E|+|V|) < ((|V|+|E|)*f)<br>
	 * </br>we are sure that the worst case running time is O((|V|+|E|)*f)
	 * </p>
	 * 
	 * @param g
	 *            The graph through which flow will travel.
	 * @param s
	 *            The source vertex.
	 * @param t
	 *            The destination vertex.
	 * @param vertexCapacities
	 *            The capacities of the vertices in g. You may assume that: for
	 *            all v, v is a key in vertexCapacities if and only f v is a
	 *            vertex in g.
	 * @return A maximal flow.
	 * 
	 * @author carlchapman
	 */
	@Override
	public Map<Pair<String, String>, Integer> maxFlowWithVertexCapacities(IGraph g, String s, String t, Map<String, Integer> vertexCapacities) {
		// O((e + v)*f)

		// If we can transform the input to this function into a graph that
		// can be input into the maxFlow function, then we can re-use that
		// method and return its result.

		// every edge only connects two vertices, and so the maximum flow
		// through an edge can only be the minimum of its two end point
		// capacities.

		// No flow can exist without an edge, so only vertices connected by
		// edges need to be examined. But we only have access to the edges
		// by the vertices so transforming the graph will take O(v+e)

		// transform the input into a new graph with edge weights:
		IGraph wg = new Graph();
		Map<Pair<String, String>, Integer> edgeCapacities = new HashMap<Pair<String, String>, Integer>();
		for (String v : g.getVertices()) {
			wg.addVertex(v);
			for (Pair<String, String> outgoingEdge : g.getOutgoingEdges(v)) {
				wg.addVertex(outgoingEdge.second);
				wg.addEdge(outgoingEdge);
				edgeCapacities.put(outgoingEdge, Math.min(vertexCapacities.get(v), vertexCapacities.get(outgoingEdge.second)));
			}
		}
		return maxFlow(wg, s, t, edgeCapacities);
	}

	/**
	 * Computes the maximum cardinality set of pairwise vertex-disjoint paths
	 * through g from s to t. Note that two paths are considered vertex-disjoint
	 * if and only if they share no vertices besides s and t.
	 * 
	 * <p>
	 * Function: After checking the trivial case where s==t, a new map of
	 * exploded edgeCapacities is created from the given graph. Here 'exploded'
	 * means that for every vertex except s and t, a new outgoing vertex which
	 * becomes the new source of all outgoing paths is created. An edge (here
	 * called a 'short edge') is created from the original to the new vertex.
	 * All edge capacities are set to 1. This prevents a vertex from being
	 * passed through by multiple paths, since the short Edge has a capacity of
	 * 1. <br>
	 * </br>The maxFlow is found using the exploded edge capacities.<br>
	 * </br>The maxFlow is used to create a new graph of only disjoint paths,
	 * and then all paths leading out of s are traced to t to create the list of
	 * disjoint paths.
	 * </p>
	 * <p>
	 * Cost: Creating the exploded graph involves iterating through the vertices
	 * to gain access to every edge, and so it takes O(|V|+|E|). Finding the
	 * maximum flow takes O(|E|*|P|), where P is the set maximal set of disjoint
	 * paths. Just as in the maxFlowWithVertexCapacities problem:<br>
	 * </br> You could say that the running time at this point is
	 * O(Max((|V|+|E|), (|E|*|P|))), but since that is not the right notation
	 * for big-oh, and we can be sure that both <br>
	 * </br> (|E|*|P|) < ((|V|+|E|)*|P|)<br>
	 * </br> AND<br>
	 * </br> (|E|+|V|) < ((|V|+|E|)*|P|)<br>
	 * </br>we are sure that the worst case running time is O((|V|+|E|)*|P|).
	 * This is the largest part of the problem, but to be complete, building the
	 * new graph using the maxFlow result is O(|E|), as is tracing the paths
	 * through the new graph. So the run time complexity turns out to the
	 * largest term mentioned here: O((|V|+|E|)*|P|).
	 * </p>
	 * 
	 * @param g
	 *            The graph which contains s, t, and all of the s-t paths.
	 * @param s
	 *            The first vertex in every path.
	 * @param t
	 *            The last vertex in every path.
	 * @return A Collection of paths through g, each of which starts at s and
	 *         ends at t, where every pair of paths in the collection are
	 *         vertex-disjoint from one another.
	 * 
	 * @author carlchapman
	 */
	@Override
	public Collection<List<String>> maxVertexDisjointPaths(IGraph g, String s, String t) {

		// return when s==t
		Collection<List<String>> disjointPaths = new ArrayList<List<String>>();
		if (s.equals(t)) {
			System.out.println("s equals t.  returning empty result from maxVertexDisjointPaths()");
			return disjointPaths;
		}

		// create a map of exploded edgeCapacities all set to 1 O(|v|+|e|)
		String uuid = "_" + UUID.randomUUID().toString();
		Map<Pair<String, String>, Integer> edgeCapacities = new HashMap<Pair<String, String>, Integer>();
		for (String vertex : g.getVertices()) {

			// put a short edge to prevent vertex sharing
			String inVertex = vertex;
			String outVertex = vertex + uuid;

			// don't put a shortEdge for s or t
			if (vertex.equals(s) || vertex.equals(t)) {
				outVertex = vertex;
			} else {
				Pair<String, String> shortEdge = new Pair<String, String>(inVertex, outVertex);
				edgeCapacities.put(shortEdge, 1);
			}

			// attach outgoing edges to outVertex
			for (Pair<String, String> edge : g.getOutgoingEdges(vertex)) {
				Pair<String, String> outEdge = new Pair<String, String>(outVertex, edge.second);
				edgeCapacities.put(outEdge, 1);
			}
		}

		// find the max flow O(|e|*|P|)
		Map<Pair<String, String>, Integer> edgeFlows = maxFlow(g, s, t, edgeCapacities);

		// use the maxFlow to build a new graph of only disjoint paths = O(|e|)
		IGraph disjointGraph = new Graph();
		for (Entry<Pair<String, String>, Integer> entry : edgeFlows.entrySet()) {
			if (entry.getValue() == 1) {
				Pair<String, String> edge = entry.getKey();
				disjointGraph.addEdge(edge);
				disjointGraph.addVertex(edge.first);
				disjointGraph.addVertex(edge.second);
			}
		}

		// trace paths from s to t
		for (Pair<String, String> edge : disjointGraph.getOutgoingEdges(s)) {
			LinkedList<String> disjointPath = new LinkedList<String>();
			disjointPath.add(s);
			recursivePathTrace(edge.second, disjointGraph, t, disjointPath, uuid);
			disjointPaths.add(disjointPath);
		}

		return disjointPaths;
	}

	// /////////////////////helpers///////////////////////

	/**
	 * Recursive path trace adds vertices to a list as it goes through a path,
	 * ignoring the faux vertices created when the graph was exploded using the
	 * uuid suffix to identify them.
	 * 
	 * @param vertex
	 *            the vertex we are currently on
	 * @param g
	 *            the graph
	 * @param t
	 *            the ending node
	 * @param disjointPath
	 *            the disjoint path is a list of vertices being used to create a
	 *            single disjoint path
	 * @param uuid
	 *            the uuid is a suffix used to identify faux vertices
	 * 
	 * @author carlchapman
	 */
	private void recursivePathTrace(String vertex, IGraph g, String t, LinkedList<String> disjointPath, String uuid) {

		// ignore the ends of shortEdges
		if (!vertex.endsWith(uuid)) {
			disjointPath.add(vertex);
		}
		if (!vertex.equals(t)) {
			Pair<String, String> edge = g.getOutgoingEdges(vertex).iterator().next();
			recursivePathTrace(edge.second, g, t, disjointPath, uuid);
		}
	}

	/**
	 * The augmentGraph function uses a FlowDFSCallback processor to augment a
	 * graph, adding residual flow using recursiveDFS until it has no residual
	 * flow left.
	 * 
	 * @param residualGraph
	 *            the residual graph
	 * @param residualEdgeFlows
	 *            the map of residual edge flows
	 * @param s
	 *            the starting vertex
	 * @param t
	 *            the terminal vertex
	 * @param processor
	 *            the processor
	 * 
	 * @author carlchapman
	 */
	private void augmentGraph(IGraph residualGraph, Map<Pair<String, String>, Integer> residualEdgeFlows, String s, String t,
			FlowDFSCallback processor) {
		TopologicalSort ts = new TopologicalSort();
		int nVertices = residualGraph.getVertices().size();
		int counter = 0;
		while (processor.hasPathTo_T()) {
			if (debug)
				System.out.println("iteration: " + counter++);
			HashMap<String, Boolean> discoveredMap = new HashMap<String, Boolean>(nVertices);
			ts.recursiveDFS(residualGraph, processor, s, discoveredMap);
		}
	}

	// //////////////////FlowDFSCallback//////////////////////////

	/**
	 * The Class FlowDFSCallback provides a means to augment a given residual
	 * flow graph to find the maximal flow, utilizing the DFSCallback interface.
	 * At its core, it builds a stack of edges as DFS wanders around the
	 * residual flow graph. If it hits T, then the stack is transformed into a
	 * list of edges making up the path to T, and the number to augment flow
	 * along that path by is determined. It then augments the residual flow
	 * graph. If T is not found, the algorithm raises a flag to show that it is
	 * done.
	 * 
	 * @author carlchapman
	 */
	class FlowDFSCallback implements DFSCallback {

		/** The residual edge flows. */
		private Map<Pair<String, String>, Integer> residualEdgeFlows;

		/** The residual graph. */
		private IGraph residualGraph;

		/** The start vertex. */
		private String start;

		/** The terminal vertex. */
		private String terminus;

		/**
		 * flag to know when there is no longer a path to T (and the
		 * augmentations are therefore complete)
		 */
		private boolean hasPathTo_T;

		/**
		 * flag to know if t has been encountered
		 */
		private boolean pushingTo_T;

		/**
		 * The path edge list used to store the path to T.
		 */
		private LinkedList<Pair<String, String>> pathEdgeList;

		private Stack<Pair<String, String>> edgeStack;

		private int augmentFlow;

		/**
		 * Instantiates a new FlowDFSCallback.
		 * 
		 * @param residualEdgeFlows
		 *            the residual edge flows
		 * @param residualGraph
		 *            the residual graph
		 * @param start
		 *            the start
		 * @param terminus
		 *            the terminus
		 * 
		 * @author carlchapman
		 */
		public FlowDFSCallback(Map<Pair<String, String>, Integer> residualEdgeFlows, IGraph residualGraph, String start, String terminus) {
			this.residualEdgeFlows = residualEdgeFlows;
			this.residualGraph = residualGraph;
			this.start = start;
			this.terminus = terminus;

			edgeStack = new Stack<Pair<String, String>>();
			augmentFlow = Integer.MAX_VALUE;
			pushingTo_T = true;
			hasPathTo_T = true;
			pathEdgeList = new LinkedList<Pair<String, String>>();
		}

		/**
		 * Checks for path to_ t.
		 * 
		 * @return true, if successful
		 */
		public boolean hasPathTo_T() {
			return hasPathTo_T;
		}

		// //////////////implemented interfaces/////////////

		@Override
		public void processDiscoveredVertex(String v) {

			if (v.equals(terminus)) {
				pushingTo_T = false;

				// we've found T, so transform the stack into a list of
				// consecutive edges forming a path.
				String destination = terminus;
				Pair<String, String> poppedEdge = null;
				while (!edgeStack.isEmpty()) {
					poppedEdge = edgeStack.pop();
					if (poppedEdge.second.equals(destination)) {
						pathEdgeList.addFirst(poppedEdge);
						destination = poppedEdge.first;

						// keep track of the minimum flow along this path
						augmentFlow = Math.min(augmentFlow, residualEdgeFlows.get(poppedEdge));
					}
				}
			}
		}

		@Override
		public void processExploredVertex(String v) {
			if (v.equals(start)) {

				// if no path to T was found, we are done
				if (pushingTo_T == true) {
					hasPathTo_T = false;
				} else {

					// do the augmentations
					for (Pair<String, String> pathEdge : pathEdgeList) {
						Integer previousWeight = residualEdgeFlows.get(pathEdge);
						if (debug)
							System.out.println("previousWeight: " + previousWeight + " augmentFlow: " + augmentFlow);

						// (check for error values)
						if (previousWeight == null && debug) {
							System.out.println("null weight in residualEdgeFlows for: (" + pathEdge.first + "," + pathEdge.second + ")");
						} else if ((previousWeight <= 0 || previousWeight < augmentFlow) && debug) {
							System.out.println("invalid weight (" + previousWeight + ") in residualEdgeFlows for: (" + pathEdge.first + ","
									+ pathEdge.second + ")");
						} else {

							// reduce/delete the edges along the path to T
							int newWeight = previousWeight - augmentFlow;
							if (newWeight == 0) {
								residualEdgeFlows.remove(pathEdge);
								residualGraph.deleteEdge(pathEdge);
								if (debug)
									System.out.println("deleting: (" + pathEdge.first + "," + pathEdge.second + ")");

							} else {
								residualEdgeFlows.put(pathEdge, newWeight);

							}
							// increase/create reverse edges
							Pair<String, String> reverseEdge = new Pair<String, String>(pathEdge.second, pathEdge.first);
							Integer reverseEdgeWeight = residualEdgeFlows.get(reverseEdge);
							if (reverseEdgeWeight == null) {
								residualEdgeFlows.put(reverseEdge, augmentFlow);
								residualGraph.addEdge(reverseEdge);
								if (debug)
									System.out.println("adding: (" + reverseEdge.first + "," + reverseEdge.second + ")");
							} else {
								residualEdgeFlows.put(reverseEdge, reverseEdgeWeight + augmentFlow);
							}
						}
					}// end of ...doing augmentations...
				}
				pathEdgeList.clear();
				pushingTo_T = true;
				augmentFlow = Integer.MAX_VALUE;
			}// end of ...if(v.equals(start))...
		}

		@Override
		public void processEdge(Pair<String, String> e) {
			if (pushingTo_T) {
				edgeStack.push(e);
				if (debug) {
					System.out.println("push: (" + e.first + "," + e.second + ")");
				}
			}
		}
	}
}
