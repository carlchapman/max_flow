package hw6;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.iastate.cs311.f13.hw6.IGraph;
import edu.iastate.cs311.f13.hw6.IGraph.Pair;
import edu.iastate.cs311.f13.hw6.ITopologicalSortAlgorithms;

public class TopologicalSort implements ITopologicalSortAlgorithms {
	/**
	 * Performs a depth-first traversal of g. Executes the appropriate callback
	 * methods in processor at the appropriate times.
	 * <p>
	 * Function: After checking the for the empty graph, a recursiveDFS is
	 * started at each unvisited vertex. This allows disconnected graphs to be
	 * searched, while only visiting each node once.
	 * </p>
	 * <p>
	 * Cost: this will take O(|V|+|E|) because it will visit each vertex and
	 * each edge exactly once.
	 * </p>
	 * 
	 * @param g
	 *            The graph to be traversed.
	 * @param processor
	 *            The callback functions that utilize the depth-first traversal.
	 * 
	 * @author carlchapman
	 */
	@Override
	public void DFS(IGraph g, DFSCallback processor) {

		// check for empty graph case
		Collection<String> vertices = g.getVertices();
		if (vertices.size() == 0) {
			System.out.println("empty graph. dfs trivially complete");
			return;
		}

		// start a recursiveDFS at each undiscovered vertex
		HashMap<String, Boolean> discoveredMap = new HashMap<String, Boolean>(vertices.size());
		Iterator<String> it = vertices.iterator();
		String currentVertex = null;
		while (it.hasNext()) {
			currentVertex = it.next();
			if (discoveredMap.get(currentVertex) == null) {
				recursiveDFS(g, processor, currentVertex, discoveredMap);
			}
		}
	}

	/**
	 * Recursive depth first search.
	 * 
	 * <p>
	 * Function: Mark vertex as discovered and then for each outgoing edge that
	 * leads to an undiscovered vertex, perform another recursiveDFS on that
	 * vertex.
	 * </p>
	 * <p>
	 * Cost: executing this method on a vertex in a connected component will
	 * take up to O(|E|) time, where E is the set of edges in the connected
	 * component. This is because every edge is processed, and there will always
	 * be a maximum of |E|+1 vertices in a connected component, perhaps fewer,
	 * and each vertex is visited only once. Note the comments about getting the
	 * list of outgoing edges in O(1) time due to the graph implementation.
	 * </p>
	 * 
	 * @param g
	 *            the graph being searched
	 * @param processor
	 *            the processor used to visit nodes at certain moments within
	 *            dfs
	 * @param vertex
	 *            the current vertex that dfs is at
	 * @param discoveredMap
	 *            the map of discovered vertices: if a node is discovered, then
	 *            there is an entry in the map, otherwise it is null (avoiding
	 *            initialization costs)
	 * 
	 * @author carlchapman
	 */
	public void recursiveDFS(IGraph g, DFSCallback processor, String vertex, HashMap<String, Boolean> discoveredMap) {

		// mark this vertex as discovered
		discoveredMap.put(vertex, true);

		// process vertex at discovery moment
		processor.processDiscoveredVertex(vertex);

		// for each outgoing edge...
		// Cost: my Graph implementation uses a map to retrieve the list of
		// outgoing edges, and so takes O(1) time to get the right list.
		for (Pair<String, String> e : g.getOutgoingEdges(vertex)) {

			// process each edge
			processor.processEdge(e);

			// call recursiveDFS on any edge leading to an undiscovered vertex
			if (discoveredMap.get(e.second) == null) {
				recursiveDFS(g, processor, e.second, discoveredMap);
			}
		}

		// process vertex at explored moment
		processor.processExploredVertex(vertex);
	}

	/**
	 * Computes a topological sort (or topological ordering) of the vertices in
	 * g.
	 * 
	 * <p>
	 * Function: Once a vertex has been explored, add it to the front of the
	 * list. This forms the list by reversing the backtrack order.
	 * </p>
	 * <p>
	 * Cost: by using a custom DFSCallback that prepends a list, topologicalSort
	 * is accomplished in the same time that it takes to do DFS: O(|V|+|E|)
	 * </p>
	 * 
	 * @param g
	 *            The graph that contains the vertices to be sorted and the
	 *            edges that constrain the possible orderings.
	 * @return A list of vertices that is a valid topological ordering of the
	 *         vertices in g.
	 * 
	 * @author carlchapman
	 */
	@Override
	public List<String> topologicalSort(IGraph g) {
		final LinkedList<String> reverseDiscoveryOrderList = new LinkedList<String>();

		class TopologicalDFSCallback implements DFSCallback {

			@Override
			public void processDiscoveredVertex(String v) {
			}

			@Override
			public void processExploredVertex(String v) {

				// once a vertex has been explored, add it to the front of the
				// list. This forms the list by reversing the backtrack order.
				reverseDiscoveryOrderList.addFirst(v);
			}

			@Override
			public void processEdge(Pair<String, String> e) {
			}
		}

		TopologicalDFSCallback processor = new TopologicalDFSCallback();
		DFS(g, processor);

		return reverseDiscoveryOrderList;
	}

	/**
	 * Computes the minimum amount of time required to complete the given set of
	 * jobs, subject to the constraint that a job j takes jobDurations[j]
	 * seconds on a single processor and must be completed before any job k
	 * where (j, k) is an edge in g. However, the scheduler has access to
	 * infinitely many processors and so can concurrently run as many
	 * independent jobs as it likes.
	 * 
	 * <p>
	 * Function: The graph is sorted using topological sort. This separates
	 * connected components into sublists with the property that everything a
	 * job depends on is to the left of it. This is also trivially true for
	 * separate connected components. <br>
	 * </br> A map is created that stores an in-progress total running time
	 * (total) for each vertex. The sorted list is iterated over once from left
	 * to right. At each vertex, all outgoing edges of a vertex are obtained,
	 * and the total for the destination vertex is examined. If it has no entry,
	 * or an entry less than the current vertex's total (including its running
	 * time), then the entry for the destination vertex is replaced with the
	 * higher value.<br>
	 * If a vertex has no outgoing edges, then it is an endpoint, so its
	 * duration is added to its entry. </br>Once the iteration over the sorted
	 * list is complete, the entries in the map of totals are iterated over to
	 * find the maximal value, and that value is returned.
	 * </p>
	 * <p>
	 * Cost: Topological sort takes O(|V|+|E|), then a list of all vertices is
	 * iterated over twice, adding O(|V|), which does not impact the running
	 * time, yielding a total running time of O(|V|+|E|).
	 * </p>
	 * 
	 * @param g
	 *            The graph that encodes the set of jobs to be completed and the
	 *            dependency relationships between them. For any pair of jobs j
	 *            and k, j must be completed before k if and only if the edge
	 *            (j, k) is in g.
	 * @param jobDurations
	 *            The number of seconds required by each job.
	 * @return The minimal number of seconds that must elapse between the
	 *         beginning of the first job and the end of the last.
	 * 
	 * @author carlchapman
	 */
	// citation (modified from):
	// http://www.geeksforgeeks.org/find-longest-path-directed-acyclic-graph/
	@Override
	public int minScheduleLength(IGraph g, Map<String, Integer> jobDurations) {

		List<String> sortedVertices = topologicalSort(g);
		Map<String, Integer> totals = new HashMap<String, Integer>(sortedVertices.size());

		Iterator<String> it = sortedVertices.iterator();
		while (it.hasNext()) {
			String vertex = it.next();
			Integer myTime = jobDurations.get(vertex);

			if (myTime == null) {
				System.out.println("null length provided for: " + vertex);
			} else {
				Collection<Pair<String, String>> outgoingEdges = g.getOutgoingEdges(vertex);
				Integer myLeadTime = totals.get(vertex);
				Integer timeThroughMe = myLeadTime == null ? myTime : myLeadTime + myTime;

				// this is an endpoint, so add your time to yourself
				if (outgoingEdges.size() == 0) {
					totals.put(vertex, timeThroughMe);

				} else {
					// add time to those depending on you, if it's larger
					for (Pair<String, String> edge : g.getOutgoingEdges(vertex)) {
						String depender = edge.second;
						Integer dependerTime = totals.get(depender);
						dependerTime = dependerTime == null ? 0 : dependerTime;

						// if the path through me is longer, change to that
						if (timeThroughMe > dependerTime) {
							totals.put(depender, timeThroughMe);
						}
					}
				}
			}
		}

		Iterator<Integer> lengthIt = totals.values().iterator();
		int max = 0;
		while (lengthIt.hasNext()) {
			int totalLength = lengthIt.next();
			if (max < totalLength) {
				max = totalLength;
			}
		}
		return max;
	}
}
