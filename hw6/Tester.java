package hw6;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import edu.iastate.cs311.f13.hw6.IGraph;
import edu.iastate.cs311.f13.hw6.IGraph.Pair;

public class Tester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		runTopologicalTests();
		runMaxFlowTests();
	}
	
	private static void runTopologicalTests(){
		TopologicalSort ts = new TopologicalSort();
		IGraph g = getTopologicalGraph();
		printList(ts.topologicalSort(g));
		Map<String,Integer> jobDurations = getJobDurations();
		System.out.println("minScheduleLength: "+ts.minScheduleLength(g, jobDurations));
	}
	
	private static void runMaxFlowTests(){
		IGraph g = getMaxFlowGraph();
		Map<Pair<String, String>, Integer> capacityMap = getCapacityMap();
		MaxFlowAlgorithms mx = new MaxFlowAlgorithms();

		System.out.println("maxFlow");
		Map<Pair<String, String>, Integer> maxFlowMap = mx.maxFlow(g, "S", "T", capacityMap);
		printEdgeMap(maxFlowMap);
		System.out.println("\nmaxFlowVertices");
		Map<Pair<String, String>, Integer> maxFlowVertices = mx.maxFlowWithVertexCapacities(getVertexCapacityGraph(), "S", "T",
				getVertexCapacity());
		printEdgeMap(maxFlowVertices);
		
		System.out.println("maxDisjointFlow");
		Collection<List<String>> results = mx.maxVertexDisjointPaths(g, "S", "T");
		for(List<String> list : results){
			printList(list);
		}
		
		
	}
	
	private static IGraph getMaxFlowGraph() {
		Graph g = new Graph();
		g.addVertex("S");
		g.addVertex("A");
		g.addVertex("B");
		g.addVertex("C");
		g.addVertex("D");
		g.addVertex("E");
		g.addVertex("F");
		g.addVertex("G");
		g.addVertex("T");
		
		g.addEdge(new Pair<String, String>("A", "B"));
		g.addEdge(new Pair<String, String>("B", "T"));
		g.addEdge(new Pair<String, String>("S", "A"));
		g.addEdge(new Pair<String, String>("B", "C"));
		g.addEdge(new Pair<String, String>("S", "C"));
		g.addEdge(new Pair<String, String>("S", "B"));
		g.addEdge(new Pair<String, String>("C", "D"));
		g.addEdge(new Pair<String, String>("D", "T"));
		
		g.addEdge(new Pair<String, String>("S", "F"));
		g.addEdge(new Pair<String, String>("S", "E"));
		g.addEdge(new Pair<String, String>("D", "B"));
		g.addEdge(new Pair<String, String>("F", "T"));
		
		g.addEdge(new Pair<String, String>("E", "T"));
		g.addEdge(new Pair<String, String>("C", "T"));
		g.addEdge(new Pair<String, String>("C", "G"));
		g.addEdge(new Pair<String, String>("G", "T"));
		
		g.addEdge(new Pair<String, String>("A", "E"));
		g.addEdge(new Pair<String, String>("E", "G"));
		g.addEdge(new Pair<String, String>("C", "T"));
		g.addEdge(new Pair<String, String>("F", "D"));

		return g;
	}
	
	private static Map<Pair<String, String>, Integer> getCapacityMap(){
		Map<Pair<String, String>, Integer> capacityMap = new HashMap<Pair<String, String>, Integer>();
		
		Pair<String, String> e1 = new Pair<String, String>("A", "B");
		Pair<String, String> e2 = new Pair<String, String>("B", "T");
		Pair<String, String> e3 = new Pair<String, String>("S", "A");
		Pair<String, String> e4 = new Pair<String, String>("B", "C");
		Pair<String, String> e5 = new Pair<String, String>("S", "C");
		Pair<String, String> e6 = new Pair<String, String>("S", "B");
		Pair<String, String> e7 = new Pair<String, String>("C", "D");
		Pair<String, String> e8 = new Pair<String, String>("D", "T");
		Pair<String, String> e9 = new Pair<String, String>("S", "F");
		Pair<String, String> e10 = new Pair<String, String>("S", "E");
		Pair<String, String> e11 = new Pair<String, String>("D", "B");
		Pair<String, String> e12 = new Pair<String, String>("F", "T");
		Pair<String, String> e13 = new Pair<String, String>("E", "T");
		Pair<String, String> e14 = new Pair<String, String>("C", "T");
		Pair<String, String> e15 = new Pair<String, String>("C", "G");
		Pair<String, String> e16 = new Pair<String, String>("G", "T");
		Pair<String, String> e17 = new Pair<String, String>("A", "E");
		Pair<String, String> e18 = new Pair<String, String>("E", "G");
		Pair<String, String> e19 = new Pair<String, String>("C", "T");
		Pair<String, String> e20= new Pair<String, String>("F", "D");

		capacityMap.put(e1, 1);
		capacityMap.put(e2, 1);
		capacityMap.put(e3, 1);
		capacityMap.put(e4, 1);
		capacityMap.put(e5, 1);
		capacityMap.put(e6, 1);
		capacityMap.put(e7, 1);
		capacityMap.put(e8, 1);
		capacityMap.put(e9, 1);
		capacityMap.put(e10, 1);
		capacityMap.put(e11, 1);
		capacityMap.put(e12, 1);
		capacityMap.put(e13, 1);
		capacityMap.put(e14, 1);
		capacityMap.put(e15, 1);
		capacityMap.put(e16, 1);
		capacityMap.put(e17, 1);
		capacityMap.put(e18, 1);
		capacityMap.put(e19, 1);
		capacityMap.put(e20, 1);

		return capacityMap;
	}

	private static void printEdgeMap(Map<Pair<String, String>, Integer> edges) {
		for (Entry<Pair<String, String>, Integer> entry : edges.entrySet()) {
			Pair<String, String> edge = entry.getKey();
			System.out.println("Entry: (" + edge.first + "," + edge.second + "): " + entry.getValue());
		}
	}

	private static void printList(List<String> list) {
		for (String s : list) {
			System.out.print(s + ",");
		}
		System.out.println();
	}


	
	private static Map<String,Integer> getJobDurations(){
		
		Map<String,Integer> jobDurations = new HashMap<String,Integer>();
		jobDurations.put("M", 2000);
		jobDurations.put("X", 50);
		jobDurations.put("Y", 12);
		jobDurations.put("A", 100);
		jobDurations.put("B", 200);
		jobDurations.put("C", 20);
		jobDurations.put("D", 5);
		jobDurations.put("E", 50);
		jobDurations.put("F", 12);
		jobDurations.put("G", 100);
		
		return jobDurations;
		
	}
	
	private static IGraph getTopologicalGraph() {
		Graph g = new Graph();
		g.addVertex("M");
		g.addVertex("X");
		g.addVertex("Y");
		
		
		g.addVertex("A");
		g.addVertex("B");
		g.addVertex("C");
		g.addVertex("D");
		g.addVertex("E");
		g.addVertex("F");
		g.addVertex("G");

		g.addEdge(new Pair<String, String>("X", "Y"));
		
		g.addEdge(new Pair<String, String>("A", "B"));
		g.addEdge(new Pair<String, String>("A", "C"));
		
		g.addEdge(new Pair<String, String>("B", "D"));
		g.addEdge(new Pair<String, String>("B", "E"));
		
		g.addEdge(new Pair<String, String>("C", "F"));
		g.addEdge(new Pair<String, String>("C", "G"));
	
		return g;
	}

	private static IGraph getVertexCapacityGraph() {
		Graph g = new Graph();
		g.addVertex("S");
		g.addVertex("A");
		g.addVertex("B");
		g.addVertex("C");
		g.addVertex("D");
		g.addVertex("E");
		g.addVertex("T");

		g.addEdge(new Pair<String, String>("S", "A"));
		g.addEdge(new Pair<String, String>("S", "E"));
		g.addEdge(new Pair<String, String>("A", "C"));
		g.addEdge(new Pair<String, String>("A", "B"));
		g.addEdge(new Pair<String, String>("C", "D"));
		g.addEdge(new Pair<String, String>("B", "D"));
		g.addEdge(new Pair<String, String>("B", "T"));
		g.addEdge(new Pair<String, String>("D", "T"));
		g.addEdge(new Pair<String, String>("E", "D"));
		g.addEdge(new Pair<String, String>("E", "B"));

		return g;
	}

	private static Map<String, Integer> getVertexCapacity() {
		Map<String, Integer> capacity = new HashMap<String, Integer>();
		capacity.put("S", 100);
		capacity.put("A", 15);
		capacity.put("C", 12);
		capacity.put("B", 10);
		capacity.put("D", 55);
		capacity.put("E", 41);
		capacity.put("T", 1000);

		return capacity;
	}
}
